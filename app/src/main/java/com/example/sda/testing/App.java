package com.example.sda.testing;

import android.app.Application;

import com.example.sda.testing.di.AppComponent;
import com.example.sda.testing.di.AppModule;
import com.example.sda.testing.di.DaggerAppComponent;

/**
 * Created by sda on 17.07.17.
 */

public class App extends Application {

    private AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerAppComponent.builder().appModule(new AppModule()).build();
    }

    public AppComponent getComponent() {
        return component;
    }
}
