package com.example.sda.testing.ui;

import com.example.sda.testing.R;

import javax.inject.Inject;

/**
 * Created by sda on 17.07.17.
 */

public class MainPresenter {

    private MainMvpView view;
    private String regexPattern = "\\d*";

    public void attachView(MainMvpView view) {
        this.view = view;
    }

    public void detachView() {
        this.view = null;
    }

    public void onButtonClick(String input) {
        if (input.isEmpty()) {
            view.showEmptyInputMessage();
            view.displayMessage(R.string.main_empty_input);
        } else if (!input.matches(regexPattern)) {
            view.showWrongInputMessage();
        } else {
            view.showText(input);
        }
    }

    public void onBttnClick() {
        String input = view.getUserInput();
        if (input.isEmpty()) {
            view.showEmptyInputMessage();
            view.displayMessage(R.string.main_empty_input);
        } else if (!input.matches(regexPattern)) {
            view.showWrongInputMessage();
        } else {
            view.showText(input);
        }
    }
}
