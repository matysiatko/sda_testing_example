package com.example.sda.testing.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.example.sda.testing.App;
import com.example.sda.testing.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements MainMvpView {

    @Inject
    MainPresenter presenter;

    @BindView(R.id.main_input)
    EditText input;

    @BindView(R.id.main_text)
    TextView text;

    @OnClick(R.id.main_button)
    public void onButtonClick() {
        presenter.onButtonClick(input.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((App)getApplication()).getComponent().inject(this);
        presenter.attachView(this);
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }

    @Override
    public void showEmptyInputMessage() {

    }

    @Override
    public void showWrongInputMessage() {

    }

    @Override
    public void showText(String input) {

    }

    @Override
    public void displayMessage(int resId) {
        text.setText(resId);
    }

    @Override
    public String getUserInput() {
        return null;
    }
}
