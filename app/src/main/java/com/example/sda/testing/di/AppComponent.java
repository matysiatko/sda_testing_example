package com.example.sda.testing.di;

import com.example.sda.testing.ui.MainActivity;

import dagger.Component;

/**
 * Created by sda on 17.07.17.
 */

@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(MainActivity mainActivity);

}
