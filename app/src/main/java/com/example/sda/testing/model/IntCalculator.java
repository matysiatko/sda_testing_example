package com.example.sda.testing.model;

/**
 * Created by sda on 15.07.17.
 */

public class IntCalculator {

    public double multiply(double a, double b) {
        return a * b;
    }

    public int divide(int value, int divider) throws ArithmeticException {
        return value / divider;
    }
}
