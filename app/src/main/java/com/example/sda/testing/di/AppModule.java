package com.example.sda.testing.di;

import com.example.sda.testing.ui.MainPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by sda on 17.07.17.
 */
@Module
public class AppModule {

    @Provides
    public MainPresenter providesMainPresenter() {
        return new MainPresenter();
    }
}
