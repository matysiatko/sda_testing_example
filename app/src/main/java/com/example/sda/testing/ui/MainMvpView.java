package com.example.sda.testing.ui;

/**
 * Created by sda on 17.07.17.
 */

public interface MainMvpView {

    void showEmptyInputMessage();

    void showWrongInputMessage();

    void showText(String input);

    void displayMessage(int resId);

    String getUserInput();
}
