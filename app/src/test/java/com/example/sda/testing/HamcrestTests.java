package com.example.sda.testing;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasItem;

/**
 * Created by sda on 15.07.17.
 */

public class HamcrestTests {

    @Test
    public void shouldCheckIfListContainsGivenElementsWithoutHamcrest() throws Exception {
        List<Integer> integers = Arrays.asList(1, 3, 5, 6);
        Integer result = -1;
        for (Integer value : integers) {
            if (value == 3) {
                result = value;
                return;
            }
        }
        assertEquals("List should contain number 3", (Integer) 3, result);
    }

    @Test
    public void shouldCheckIfListContainsGivenElementsWithHamcrest() throws Exception {
        List<Integer> integers = Arrays.asList(1, 3, 5, 6);
        assertThat("List should contain number 3", integers, hasItem(3));
    }
}
