package com.example.sda.testing;

import android.content.Intent;

import com.example.sda.testing.model.IntCalculator;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by sda on 15.07.17.
 */

public class IntCalculatorTest {

    private IntCalculator intCalculator;

    @Before
    public void setUp() throws Exception {
        intCalculator = new IntCalculator();

    }

    @Test
    public void shouldDivideCorrectly() {
        double result = intCalculator.divide(5, 2);
        double expected = 2;
        assertEquals("Should divide correctly", expected, result, 0);
    }

    @Test(expected = ArithmeticException.class)
    public void shouldCatchExceptionWhileDividingByZero() {
        intCalculator.divide(5, 0);
    }

    @Test
    public void shouldReturnZeroWhileMultiplyingByZero() throws Exception {
        double result = intCalculator.multiply(5, 0);
        double result2 = intCalculator.multiply(0, 5);
        double expected = 0;
        assertThat(result, is(expected));
        assertThat(result2, is(expected));
    }
}
