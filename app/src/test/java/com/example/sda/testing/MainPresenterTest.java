package com.example.sda.testing;

import com.example.sda.testing.ui.MainMvpView;
import com.example.sda.testing.ui.MainPresenter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by sda on 17.07.17.
 */
@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {

    @Mock
    MainMvpView view;

    private MainPresenter presenter;

    @Before
    public void setup() {
        presenter = new MainPresenter();
        presenter.attachView(view);
    }

    @Test
    public void viewShouldShowEmptyStringMessage() {
        String input = "";
        presenter.onButtonClick(input);
        verify(view).showEmptyInputMessage();
    }

    @Test
    public void givenEmptyStringViewShouldDisplayEmptyStringMessage() {
        // given
        when(view.getUserInput()).thenReturn("");
        // then
        presenter.onBttnClick();
        // verify
        verify(view).showEmptyInputMessage();
    }

}